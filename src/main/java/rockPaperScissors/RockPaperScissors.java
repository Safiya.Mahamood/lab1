package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {

        winnerPair = new ArrayList<>();
        winnerPair.add(new Pair("rock", "scissors"));
        winnerPair.add(new Pair("scissors", "paper"));
        winnerPair.add(new Pair("paper", "rock"));

        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    static ArrayList<Pair> winnerPair;
    
    public void run() {
        boolean gameIsOn = true;
        while (gameIsOn){
            System.out.println("Let's play round " + roundCounter);
            playerRound();
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            gameIsOn = continuePlaying();
            roundCounter++;
        }
    }

    private boolean continuePlaying() {
        while(true) {
            System.out.println("Do you wish to continue playing? (y/n)?");
            String continueGame = sc.next();
            if (continueGame.equals("y")) {
                return true;
            } else if (continueGame.equals("n")) {
                System.out.println("Bye bye :)");
                return false;
            }
            System.out.println("Owo detected wrong input");
        }
    }

    private void playerRound() {
        humanInput = getHumanInput();
        compMove = getComputerInput();
        findWinner();
    }

    private void findWinner() {
        if(winnerPair.contains(new Pair(humanInput, compMove))){
            System.out.println("Human chose " + humanInput + ", computer chose " + compMove + ". Human wins");
            humanScore++;
        }
        else if(winnerPair.contains(new Pair(compMove, humanInput))){
            System.out.println("Human chose " + humanInput + ", computer chose " + compMove + ". Computer wins");
            computerScore++;
        }
        else {
            System.out.println("Human chose " + humanInput + ", computer chose " + compMove + ". Its a tie");
        }
    }

    String humanInput = "";
    private String getHumanInput() {
        while (true) {
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String choice = sc.next();
            if (rpsChoices.contains(choice)){
                humanInput = choice;
            }
            else {
                System.out.println("I do not understand" + humanInput + ". Could you try again?");
                continue;
            }
            return humanInput;
        }
    }

    String compMove = "";
    private String getComputerInput() {
        Random rand = new Random();
        int bound = 3;
        int comInput = rand.nextInt(bound);
        if(comInput == 0){
            compMove = "rock";
        }
        else if (comInput == 1){
            compMove = "paper";
        }
        else{
            compMove = "scissors";
        }
        return compMove;
    }
}
