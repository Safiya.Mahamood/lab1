package rockPaperScissors;

public class Pair {
    String humanChoice;
    String computerChoice;

    public Pair(String humanChoice, String computerChoice){
        this.humanChoice = humanChoice;
        this.computerChoice = computerChoice;
    }

    @Override
    public boolean equals(Object other){
        Pair o = (Pair) other;
        return this.humanChoice.equals(o.humanChoice) && this.computerChoice.equals(o.computerChoice);
    }
}
